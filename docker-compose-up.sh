#!/usr/bin/env bash
cd laradock
docker-compose up -d nginx postgres pgadmin redis beanstalkd beanstalkd-console workspace php-fpm
